require('dotenv').config();

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const path = require('path');
const brain = require('brain.js');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (_, res) => {
    res.sendFile(path.join(__dirname, './public/index.html'));
});

function buildSample() {
    const nearDistances = [2200, 2100, 2300];
    const maxAltitude = 170;

    let result = [];

    for (let i = 0; i < nearDistances.length; i++) {
        const nearDistance = nearDistances[i];

        for (let aa = 0; aa < maxAltitude; aa++) {
            let obj = {
                input: [],
                output: {
                    jump: 1
                }
            };

            obj.input.push(nearDistance / 2500, aa / maxAltitude);
            result.push(obj);
        }
    }

    return result;
}

const net = new brain.NeuralNetwork({
    inputSize: 2,
    outputSize: 1,
    activation: 'sigmoid'
});

const sample = buildSample();

let trainData = Array(sample.length);

for (let i = 0; i < sample.length; i++) {
    let item = sample[i];

    item = {
        input: item.input,
        output: {
            jump: 1
        }
    };

    if (!trainData.includes(item)) {
        trainData[i] = item;
    }

    i++;

    if (sample[i]) {
        trainData[i] = {
            input: [(item.input[0] - (150 / 2500)) / 2500, item.input[1] / 180],
            output: {
                jump: 0
            }
        }
    }
}

io.on('connection', (socket) => {
    net.train(trainData, {
        errorThresh: 0.01,
        activation: 'sigmoid',
        log: true,
        logPeriod: 100
    });

    console.log('new connection', socket.id);

    socket.emit('feedback', {
        desc: 'Training...'
    });

    socket.on('projectile:pos', (data) => {
        let result = net.run([data.projectileX / 2500, data.projectileY / 180]);
        // console.log(result);

        socket.emit('feedback', {
            desc: 'Network activation: ' + JSON.stringify(result)
        });

        socket.emit('character:jump', {
            jump: result.jump >= 0.9 ? 1 : 0
        });
    });
});

server.listen(process.env.PORT, () => {
    console.log('[Server] Started on port', process.env.PORT.toString());
});