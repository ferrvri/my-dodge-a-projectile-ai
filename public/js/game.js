function between(x, min, max) {
    return x >= min && x <= max;
}

class Game {
    mySteps = JSON.parse(localStorage.getItem('mySteps')) || [];
    Score = 0;
    Scene = [];
    Feedback = [];
    State = 'ENDED';

    createProjectile(id, y, velocity = 100) {
        const _projectile = document.createElement('div');
        _projectile.id = id;
        _projectile.classList.add('projectile');
        _projectile.style.bottom = y + 'px';
        _projectile.style.right = 0;

        _projectile.selfInterval = setInterval(() => {
            const actualRight = +(_projectile.style.right.replace(/px/g, ''));
            _projectile.style.right = actualRight + velocity + 'px';

            this.checkHitBox(id, actualRight + velocity, y, state => {
                if (state == 'checkbox:end') {
                    clearInterval(_projectile.selfInterval);

                    if (this.State != 'ENDED') {
                        this.spawnProjectile();
                        this.render();
                    } else {
                        const playAgainButton = document.querySelector('#playagain');
                        playAgainButton.style.display = 'block';
                    }
                }
            });
        }, 100);

        return _projectile;
    }

    checkHitBox(id, projectileX, projectileY, state) {
        const gameBox = document.querySelector('.game');
        const characterStatic = document.querySelector('.character');
        const characterX = +(characterStatic.style.left.replace(/px/g, ''));
        const characterYHead = +(characterStatic.style.bottom.replace(/px/g, '')) + characterStatic.clientHeight;
        const characterYFromFloor = +(characterStatic.style.bottom.replace(/px/g, ''));

        console.log('checking', characterX, characterYFromFloor, characterYHead, projectileX, projectileY)

        socket.emit('projectile:pos', { projectileX, projectileY });

        if (between(projectileY, characterYFromFloor, characterYHead) && projectileX >= (gameBox.clientWidth - characterStatic.clientWidth)) {
            if (this.mySteps) {
                console.log(JSON.parse(localStorage.getItem('mySteps')))
            }

            alert('Game over!');
            this.State = 'ENDED';
            this.destroyProjectile(id);
            state('checkbox:end');
        }

        if (projectileX >= gameBox.clientWidth) {
            this.destroyProjectile(id);
            // state('checkbox:end');
            window.location.reload();
        }
    }

    destroyProjectile(id) {
        const projectile = document.querySelector(`#${id}`)
        const projectileDOMObject = document.querySelector('#projectiles');
        projectileDOMObject.removeChild(projectile);
    }

    spawnProjectile() {
        // ${Math.random().toString(16).substring(2)}
        const projectile = new GameObject(`projectile`, 50, 25, 'projectile');
        this.Scene.push(projectile);
    }

    startGame() {
        this.State = 'STARTED';
        this.spawnProjectile();
        this.render();

        window.addEventListener('keydown', keydownEvent => {
            if (keydownEvent.key == 'w') {
                this.characterJump();
                const actualProjectile = document.querySelector('#projectile');

                this.mySteps.push([
                    +actualProjectile.style.right.replace(/px/g, ''),
                    +actualProjectile.style.bottom.replace(/px/g, '')
                ]);

                localStorage.setItem('mySteps', JSON.stringify(this.mySteps));
            }
        });

        socket.on('character:jump', (jumpData) => {
            if (jumpData.jump) {
                this.characterJump();
            }
        });

        socket.on('feedback', (feedbackData) => {
            this.Feedback.unshift(feedbackData);

            const feedbackBox = document.querySelector('#feedback');
            feedbackBox.innerHTML = '';

            this.Feedback.forEach(feedback => {
                feedbackBox.innerHTML += `<p>${feedback.desc}</p><br>`;
            })
        });
    }

    characterJump() {
        const characterStatic = document.querySelector('.character');
        characterStatic.style.bottom = '190px';

        setTimeout(() => {
            characterStatic.style.bottom = '0px';
        }, 1500);
    }

    render(launcherSize = 200) {
        const projectileDOMObject = document.querySelector('#projectiles');

        this.Scene.forEach(item => {
            if (item._type == 'projectile') {
                const _projectile = this.createProjectile(item._id, Math.round(Math.random() * launcherSize));
                projectileDOMObject.append(_projectile);
            }
        });
    }
}

window.addEventListener('DOMContentLoaded', (domContentLoaderEvent) => {
    socket.connect();
    const game = new Game();
    game.startGame();
});