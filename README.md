# my-dodge-a-projectile-ai

## 🇺🇲
A proof of concept that aims to apply knowledge of decision making in Artificial Intelligence using Javascript and the frameworks:

Brain.js (Neural network)
Express + Socket.io (Server & Messaging)

Through a simulation of a projectile walking towards the main player.

The neural network needs to identify the training parameters, and apply the decision making at the right time, to jump or not to jump.

I confess that some lines of code are not at 100%, for example I am mocking the values 2500 and 180 that refer to the size of the screen and the size of the player in relation to the screen.
But for now I'm just applying a proof of concept as agile as possible 😁.

Thank you!

## 🇧🇷
Uma prova de conceito que visa aplicar o conhecimento de tomada de decisão em Inteligencia Artificial usando Javascript e os frameworks:

Brain.js (Rede neural)
Express + Socket.io (Servidor & Mensageria)

Atraves de uma simulação de um projetil caminhando em direção ao player principal.

A rede neural precisa identificar os parametros de treinamento, e aplicar a tomada de decisão  no tempo correto, pular ou não pular.

Confesso que algumas linhas de codigo nao estão nos 100%, como por exemplo estou mockando os valores 2500 e 180 que fazem referencia ao tamanho da tela e o tamanho do player em relacao a tela.
Mas por enquanto estou apenas aplicando uma prova de conceito mais agil possivel que consigo 😁.

Obrigadooo!



## **Setup**
```
npm i
node index.js
```